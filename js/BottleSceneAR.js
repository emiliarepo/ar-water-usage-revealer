'use strict';

import React, { Component, useState} from 'react';
import {
  View,
  Text,
  Div
} from 'react-native';

import {
  ViroARScene,
  ViroText,
  ViroConstants,
  Viro3DObject,
  ViroAmbientLight,
  ViroARPlane,
  ViroARPlaneSelector,
  ViroMaterials
} from 'react-viro';

import {
  apartmentKeys, listOfTotalConsumptions, listOfTotalPowerConsumptions, listOfTotalConsumptionsPerFaucet
} from './../apartments'

import SelectionContext from './../context'

ViroMaterials.createMaterials(
  { 
    basic: {
      lightingModel: "Blinn",
      diffuseColor: '#FFFFFF'
    }
  }
)

export default class BottleSceneAR extends Component {

  constructor() {
    super();

    // Set initial state here
    this.placeSelected = this.context
    // bind 'this' to functions
    this._onInitialized = this._onInitialized.bind(this);
  }

  createBottles(placeSelected) {
    let bottlesObj =[require('./res/bottle1.obj'), require('./res/bottle2.obj'), require('./res/bottle3.obj'), require('./res/bottle4.obj'), require('./res/bottle5.obj')]
    let bottlesMtl =[require('./res/bottle1m.mtl'), require('./res/bottle2m.mtl'), require('./res/bottle3m.mtl'), require('./res/bottle4m.mtl'), require('./res/bottle5m.mtl')]  
    
    let bottles = []
    for (let i = 0; i < listOfTotalConsumptionsPerFaucet[placeSelected].length; i++) {
        for (let j = 0; j < Math.ceil(listOfTotalConsumptionsPerFaucet[placeSelected][i] / 1.5); j++) {
            bottles.push(i)
        }
    }

    let cubeSide = Math.floor(Math.cbrt(bottles.length));

    let x = 0
    let components = []
    let breakOut = false
    for (let i = 0; i < 1e9; i++) {
        if (breakOut) break
        for (let j = 0; j < cubeSide; j++) {
            if (breakOut) break
            for (let k = 0; k < cubeSide; k++) {
                if (breakOut) break
                let bottle = bottles[x]
                if (bottle === undefined) { breakOut = true; break; };
                components.push(<Viro3DObject source={bottlesObj[bottle]}
                  key={i * cubeSide * cubeSide + j * cubeSide + k}
                  renderingOrder={i * cubeSide * cubeSide + j * cubeSide + k}
                  position={[0 + i * 0.12, 0 + j * 0.32, 0 + k * 0.12]}
                  resources={[bottlesMtl[bottle]]}
                  scale={[0.00032, 0.00032, 0.00032]}
                  type="OBJ" />)
                x++
            }
        }
    }

    return components;
  }

  createTurve(placeSelected) {
    let size = Math.cbrt((listOfTotalPowerConsumptions[placeSelected] * 3.6) / 11.9 / 400)
    return <Viro3DObject source={require("./res/pyramid.obj")}
            key={-1}
            renderingOrder={-1}
            position={[0, 0, 0]}
            resources={[require("./res/pyramidm.mtl")]}
            scale={[size, size, size]}
            type="OBJ" />
  }

//onSelected={() => setState({...this.state, isSelected: true})}
  render() {
    let placeSelected = this.context.selectedApartment
    let resourceSelected = this.context.selectedResource
    console.log('place selected in BottleSceneAR: '+ placeSelected)
    return (  
      <ViroARScene onTrackingUpdated={this._onInitialized} >
        <ViroAmbientLight color="#FFFFFF" />
        <ViroARPlane  minHeight={.5} minWidth={.5} alignment={"Horizontal"}>
           {resourceSelected == 0  ? this.createBottles(placeSelected) :
           this.createTurve(placeSelected) }
        </ViroARPlane>
      </ViroARScene>
    );
  }

  _onInitialized(state, reason) {
    if (state == ViroConstants.TRACKING_NORMAL) {

    } else if (state == ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }
}

BottleSceneAR.contextType = SelectionContext
module.exports = BottleSceneAR;
