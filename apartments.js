import waterData from './db.json';

const apartmentKeys = Array.from({length:waterData["houses"][0]["apartments"].length},(v,k)=>k+1)


var apartments = waterData.houses[0].apartments
var listOfTotalConsumptions = apartments.map(x => Object.values(x))
listOfTotalConsumptions = listOfTotalConsumptions.map(x => x.filter(x => x.hasOwnProperty('measurements')))
listOfTotalConsumptions = listOfTotalConsumptions.map(x => x.map(x => x.measurements))


var listOfTotalPowerConsumptions = listOfTotalConsumptions.map(x => x.map(x => x.map(x => parseFloat(x.Power_Consumption))))
listOfTotalPowerConsumptions = listOfTotalPowerConsumptions.map(x => x.map(x => x.reduce((a,b) => a+b)))
listOfTotalPowerConsumptions = listOfTotalPowerConsumptions.map(x => Math.floor(x.reduce((a,b) => a+b) / 52))

listOfTotalConsumptions = listOfTotalConsumptions.map(x => x.map(x => x.map(x => parseFloat(x.Consumption))))
listOfTotalConsumptions = listOfTotalConsumptions.map(x => x.map(x => x.reduce((a,b) => a+b)))

var listOfTotalConsumptionsPerFaucet = listOfTotalConsumptions.map(x => x.map(x => Math.floor(x / 365)))


listOfTotalConsumptions = listOfTotalConsumptions.map(x => Math.floor(x.reduce((a,b) => a+b) / 365))


module.exports = {apartmentKeys, listOfTotalConsumptions, listOfTotalConsumptionsPerFaucet, listOfTotalPowerConsumptions}