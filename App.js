/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  StyleSheet,
  PixelRatio,
  TouchableHighlight,
  ImageBackground,
  Button,
  ScrollView
} from 'react-native';

import {
  ViroVRSceneNavigator,
  ViroARSceneNavigator
} from 'react-viro';

import SelectDropdown from 'react-native-select-dropdown'

import {
  apartmentKeys, listOfTotalConsumptions, listOfTotalPowerConsumptions
} from './apartments'

/*
 TODO: Insert your API key below
 */
var sharedProps = {
  apiKey:"API_KEY_HERE",
}

// Sets the default scene you want for AR and VR
var InitialARScene = require('./js/BottleSceneAR');

var UNSET = "UNSET";
var VR_NAVIGATOR_TYPE = "VR";
var AR_NAVIGATOR_TYPE = "AR";

// This determines which type of experience to launch in, or UNSET, if the user should
// be presented with a choice of AR or VR. By default, we offer the user a choice.
var defaultNavigatorType = UNSET;

import SelectedApartmentContext from './context'

export default class WaterConsumption extends Component {
  constructor() {
    super();

    this.state = {
      navigatorType : defaultNavigatorType,
      sharedProps : sharedProps,
      apartmentNumber: 0,
      selectedResource: 0
    }
    this._getExperienceSelector = this._getExperienceSelector.bind(this);
    this._getARNavigator = this._getARNavigator.bind(this);
    this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(this);
    this._exitViro = this._exitViro.bind(this);

  }

  // Replace this function with the contents of _getVRNavigator() or _getARNavigator()
  // if you are building a specific type of experience.
  render() {
    if (this.state.navigatorType == UNSET) {
      return this._getExperienceSelector();
    } else if (this.state.navigatorType == AR_NAVIGATOR_TYPE) {
      return this._getARNavigator();
    }
  }

  // Presents the user with a choice of an AR or VR experience
  _getExperienceSelector() {
    return (
      <ImageBackground source={{uri: "https://wallpapercave.com/wp/wp5103646.jpg"}} resizeMode="cover" style={localStyles.image}>
        <View style={localStyles.outer} >
          <View style={localStyles.inner} >

            <View style={localStyles.textContainer}>
              <Text style={localStyles.titleText}>
                Water Consumption
              </Text>
              <Text style={localStyles.subtitleText}>
                Visualise your water consumption data
              </Text>
            </View>


            <SelectDropdown
            onSelect={(itemValue, itemIndex) =>
              this.setState({
                ...this.state,
                apartmentNumber: itemIndex
              })}
              data={apartmentKeys}
            />

            <SelectDropdown
            style={{marginTop: 20}}
            onSelect={(itemValue, itemIndex) =>
              this.setState({
                ...this.state,
                selectedResource: itemIndex
              })}
              data={["Water per day", "Peat per week"]}
            />
            
            <TouchableHighlight style={localStyles.buttons}
              onPress={this._getExperienceButtonOnPress(AR_NAVIGATOR_TYPE)}
              underlayColor={'#68a0ff'} >

              <Text style={localStyles.buttonText}>See in AR</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ImageBackground>
    );
  }


  _getColorDisplay(textString, color) {
    return <View style={{flex:1}}>
    <Text style={{
      backgroundColor: color,
      color: '#000',
      padding: 8, 
      marginRight: 5,
      marginLeft: 5,
      borderRadius: 10
    }}>{textString}</Text>
    </View>
  }
  

  // Returns the ViroARSceneNavigator which will start the AR experience
  _getARNavigator() {
    return (
      <>
      <SelectedApartmentContext.Provider value={{selectedApartment: this.state.apartmentNumber, selectedResource: this.state.selectedResource }}>
        <ViroARSceneNavigator {...this.state.sharedProps}
        initialScene={{scene: InitialARScene}} />
        <View style={{paddingBottom: 20}}>
          <View style = {localStyles.textContainer}>
            <Text style = {localStyles.titleText}>{this.state.selectedResource == 0 ? "Water" : "Power"} Consumption for Apartment {this.state.apartmentNumber+1}</Text>
            {
            this.state.selectedResource == 0 ?
            <>
              <Text style = {localStyles.subtitleText}>This household consumes {listOfTotalConsumptions[this.state.apartmentNumber]} liters of water per day.</Text>
              <Text style = {localStyles.subtitleText}>This view visualizes it as {Math.floor(listOfTotalConsumptions[this.state.apartmentNumber] / 1.5)} 1.5L water bottles.</Text>
            </>
            :
            <>
              <Text style = {localStyles.subtitleText}>Heating the water consumes {Math.floor(listOfTotalPowerConsumptions[this.state.apartmentNumber])}kWh per week.</Text>
              <Text style = {localStyles.subtitleText}>It is equivalent to e.g. {Math.floor(listOfTotalPowerConsumptions[this.state.apartmentNumber] * 3.6 / 11.9 * 10) / 10}kg of peat.</Text>
              <Text style = {localStyles.subtitleText}>The amount of peat burned results in {Math.floor(listOfTotalPowerConsumptions[this.state.apartmentNumber] * 29.4)}g of CO₂ a week.</Text>
            </>
            }
          </View>
          { this.state.selectedResource == 0 ? (
          <View style = {localStyles.textContainer}>
            <View style={{flexDirection:"row"}}>
              {this._getColorDisplay("Dishwasher", "rgb(204, 255, 204)")}
              {this._getColorDisplay("Hydractiva Shower", "rgb(102, 153, 255)")}
              {this._getColorDisplay("Optima Kitchen", "rgb(51, 102, 204)")}
              {this._getColorDisplay("Optima Bathroom", "rgb(51, 153, 102)")}
              {this._getColorDisplay("Washing Machine", "rgb(204, 102, 153)")}
            </View>
          </View>)
             : <></>
          }
          <Button onPress={this._getExperienceButtonOnPress(UNSET)} style={localStyles.exitButton} title="Back" />
        </View>
      </SelectedApartmentContext.Provider>
      </>
    );
  }

  // This function returns an anonymous/lambda function to be used
  // by the experience selector buttons
  _getExperienceButtonOnPress(navigatorType) {
    console.log('updated nagivator type')
    return () => {
      this.setState({
        ...this.state,
        navigatorType : navigatorType
      })
    }
  }

  // This function "exits" Viro by setting the navigatorType to UNSET.
  _exitViro() {
    this.setState({
      navigatorType : UNSET
    })
  }
}

var localStyles = StyleSheet.create({
  viroContainer :{
    flex : 1,
    backgroundColor: "black",
  },
  outer : {
    flex : 1,
    flexDirection: 'row',
    alignItems:'center',
  },
  inner: {
    flex : 1,
    flexDirection: 'column',
    alignItems:'center',
    backgroundColor: "white",
    borderRadius: 20,
    opacity: 0.8,
    marginLeft: 60,
    marginRight: 60
  },
  titleText: {
    paddingTop: 30,
    paddingBottom: 10,
    opacity: 1,
    color:'#000',
    textAlign:'center',
    fontSize : 25
  },
  subtitleText: {
    paddingTop: 5,
    paddingBottom: 0,
    opacity: 1,
    color:'#000',
    textAlign:'center',
    fontSize : 20
  },
  buttonText: {
    color:'#fff',
    textAlign:'center',
    opacity: 1,
    fontSize : 20
  },
  buttons : {
    height: 80,
    width: 150,
    paddingTop:27,
    paddingBottom:20,
    marginTop: 30,
    marginBottom: 30,
    backgroundColor:'#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    opacity: 1
  },
  exitButton : {
    height: 50,
    width: 100,
    paddingTop:10,
    paddingBottom:10,
    marginTop: 60,
    marginBottom: 25,
    backgroundColor:'#68a0cf',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  textContainer: {
    marginBottom: 30
  }
});
